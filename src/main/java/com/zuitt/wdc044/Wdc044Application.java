package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" is an example of "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
 // These are used to manage and configure the behavior of the framework.
 // Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
 // Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

@SpringBootApplication

// This will indicate that a class is a controller that will handle RESTful web requests and returns an HTTP response.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
	// This method starts the whole Spring Framework.
	// This serves as the entry point to start the application.
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used to map HTTP GET requests.
		// Note: HTTP Request annotations is usually followed by a "method body"
		// The "method body" contains the logic to generate the response.
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters, form parameters, and even files from the request.
		// if the URL is: /hello?name=john the method will return "Hello john"
			// "?" means that the start of the parameters followed by the "key=value" pair.
		// if the URL is: /hello, the method will return I "Hello World."
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		// To send a response of "Hello + name"
		return String.format("Hello %s!", name);

	}

//	Activity s01
	// localhost:8080/hi?user=Ivan

	@GetMapping("/hi")

	public String hi(@RequestParam(value = "user", defaultValue = "user!") String user){

		return String.format("Hi %s!", user);

	}


//	Activity Stretch Goal s01:
	// localhost:8080/nameAge?user=Ivan&age=26

	@GetMapping("/nameAge")

	public String nameAge(@RequestParam(value = "user", defaultValue = "user!") String user,
					 @RequestParam(value = "age", defaultValue = "1") String age){

		return String.format("Hi %s! Your age is %s.", user, age);

	}




}
