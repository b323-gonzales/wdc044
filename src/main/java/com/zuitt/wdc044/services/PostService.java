package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.stereotype.Service;
import java.util.List;import org.springframework.http.ResponseEntity;
import java.util.List;


public interface PostService {

    void createPost(String stringToken, Post post);
    ResponseEntity updatePost (Long id, String stringToken, Post post);
    ResponseEntity deletePost (Long id, String stringToken);
    Iterable<Post> getPosts();

    // New method for retrieving user's posts
    List<Post> getPostsByUser(String stringToken);

}


