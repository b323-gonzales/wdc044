package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

// an interface contains behaviour that a class implements
// an interface marked as @Repository contains methods for database manipulation
// by extending Crud Repository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating, and deleting
public interface PostRepository extends CrudRepository<Post,Object> {
    List<Post> findByUser(User user);
}