package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.zuitt.wdc044.config.JwtToken;

@RestController
//Enable cross origin requests via @CrossOrigin
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;


    //map web requests to controller methods via @RequestMapping
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

//    Get user's Post
    @RequestMapping(value="/myPosts", method = RequestMethod.GET) // New endpoint for retrieving user's posts
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getPostsByUser(stringToken), HttpStatus.OK);
    }

    //Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }

    @RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }


}



//package com.zuitt.wdc044.controllers;
//
//import com.zuitt.wdc044.models.Post;
//import com.zuitt.wdc044.services. PostService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//// Enable cross origin requests via @CrossOrigin
//@CrossOrigin
//public class PostController {
//
//    @Autowired
//    PostService postService;
//
//    //map web requests to controller methods via @RequestMapping no usages new*
//    @RequestMapping (value="/posts", method = RequestMethod.GET)
//        public ResponseEntity getPosts() {
//        return new ResponseEntity (postService.getPosts(), HttpStatus.OK);
//    }
//
//
//    // Create new Post
//    @RequestMapping(value = "/posts", method = RequestMethod.POST)
//    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
//        postService.createPost(stringToken, post);
//        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
//    }
//
//    // Retrieve all posts
//    @GetMapping("/posts")
//    public ResponseEntity<Iterable<Post>> getAllPosts(@RequestHeader(value = "Authorization") String stringToken) {
//        Iterable<Post> posts = postService.getPosts();
//        return new ResponseEntity<>(posts, HttpStatus.OK);
//    }
//
//
//}
